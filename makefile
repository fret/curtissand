# Makefile for Website
#
# https://bitbucket.org/fret/website.git
#
# Copyright 2018-2020 Curtis Sand
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# DEPLOY_DEST: change this to use the deploy targets to deploy the website.
# HOST: use localhost for local testing, use your domain for production.
# IMAGES var should end in "images" to be compatible
# To customize the makefile targets, make a copy of "website/make.d" in your
# project's repository and update MAKE_DIR.

TITLE = Curtis Sands Website

HOST = http://curtissand.com
DEPLOY_DEST = curtsan2@curtissand.com:~/www/

BUILD = $$(pwd)/build
DIST = $$(pwd)/dist

SRC = $$(pwd)/src
HTML = $(SRC)/pages
IMAGES = $$(pwd)/images
STATIC = $(SRC)/static
STATIC_DIST = $(DIST)

PYVENV = $$(pwd)/pyvenv
PYTHON_REQUIRES = $$(pwd)/requirements.txt
NODE_PKG = $$(pwd)
NODE = $$(pwd)/node_modules

CONTAINER_OPTS = --rm -p 8888:80 -v $(DIST):/usr/share/nginx/html:Z -v $$(readlink $(IMAGES)):/usr/share/nginx/html/images:Z
PODMAN_CMD = podman run $(CONTAINER_OPTS) docker.io/library/nginx:latest
DOCKER_CMD = sudo docker run $(CONTAINER_OPTS) nginx:latest

SOURCES = $$(pwd)/sources
SOURCES_BUILD = $(BUILD)/sources
SOURCES_DIST = $(DIST)/sources
SOURCES_CMD = $$(pwd)/bin/sources_obsidian.sh
SOURCES_EXTRA_CLEAN = $$(pwd)/images

PAGES_BUILD = $(BUILD)/pages
PAGES_DIST = $(DIST)/pages
PAGES_DISPATCH = $(SRC)/skabelon/posts.py

RST_SOURCES = $(SOURCES_BUILD)
SKABELON_DISPATCH = $$(pwd)/src/skabelon/static_pages.py

CSS = $(SRC)/css
CSS_BUILD = $(BUILD)/css
CSS_DIST = $(DIST)/css

JS = $(SRC)/js
JS_DIST = $(DIST)/js
ESLINT = $(NODE)/eslint/bin/eslint.js

JQUERY_JS = $(NODE)/jquery/dist/jquery.min.js

BOOTSTRAP_SCSS = $(NODE)/bootstrap/scss
BOOTSTRAP_JS = $(NODE)/bootstrap/dist/js/bootstrap.min.js*

LIGHTBOX_CSS = $(NODE)/lightbox2/dist/css/lightbox.min.css
LIGHTBOX_IMAGES = $(NODE)/lightbox2/dist/images/*
LIGHTBOX_JS = $(NODE)/lightbox2/dist/js/lightbox.min.*

RSYNC_OPTS = -hrptLP --stats --no-whole-file --inplace -e 'ssh -p 2233'

MAKE_DIR = make.d

include $(MAKE_DIR)/install.mk
include $(MAKE_DIR)/deploy-no-images.mk
include $(MAKE_DIR)/deploy-images.mk
include $(MAKE_DIR)/run.mk
include $(MAKE_DIR)/all.mk
include $(MAKE_DIR)/rebuild.mk
include $(MAKE_DIR)/clean.mk
include $(MAKE_DIR)/build.mk
include $(MAKE_DIR)/dist.mk
include $(MAKE_DIR)/files.mk
include $(MAKE_DIR)/sources.mk
include $(MAKE_DIR)/rst.mk
include $(MAKE_DIR)/html-skabelon.mk
include $(MAKE_DIR)/pages.mk
include $(MAKE_DIR)/css.mk
include $(MAKE_DIR)/js.mk
include $(MAKE_DIR)/lightbox.mk

# Compile a Help Text with messages about each target.
HELP_TXT = \
	$(install-help)\n\
	$(install-npm-help)\n\
	$(install-py-help)\n\
	$(python-venv-help)\n\
	$(deploy-help)\n\
	$(deploy-images-help)\n\
	$(run-help)\n\
	$(all-help)\n\
	$(rebuild-help)\n\
	$(clean-help)\n\
	$(clean-all-help)\n\
	$(clean-build-help)\n\
	$(clean-dist-help)\n\
	$(clean-npm-help)\n\
	$(clean-py-help)\n\
	$(clean-js-help)\n\
	$(clean-css-help)\n\
	$(build-help)\n\
	$(dist-help)\n\
	$(static-help)\n\
	$(images-help)\n\
	$(link-sources-help)\n\
	$(build-sources-help)\n\
	$(dist-sources-help)\n\
	$(clean-sources-help)\n\
	$(compile_rst-help)\n\
	$(html-help)\n\
	$(build-pages-help)\n\
	$(dist-pages-help)\n\
	$(css-help)\n\
	$(cssdist-help)\n\
	$(css-compile-help)\n\
	$(css-prefix-help)\n\
	$(js-help)\n\
	$(jsdist-help)\n\
	$(jquery-help)\n\
	$(bootstrap-help)\n\
	$(jslint-help)\n\
	$(lightbox-help)\n

include $(MAKE_DIR)/help.mk
