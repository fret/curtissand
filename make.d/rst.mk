# Makefile for Website
#
# https://bitbucket.org/fret/website.git
#
# Copyright 2018-2020 Curtis Sand
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# RST Module : Compile a RST sources into HTML.

# To enable this module in your project add the dependencies listed below to
# your other existing targets and add the variables listed below to your top
# level makefile.
#
# Dependencies:
#
# If the Sources Module is in use, use:
#
#   - build-sources : compile-rst
#
# Otherwise use:
#
#   - all : compile-rst
#
# Variables:
#
# If the Sources Module is in use, use:
#
#   - RST_SOURCES = $(SOURCES_BUILD)
#
# Otherwise use:
#
#   - RST_SOURCES = $(HTML)



compile-rst-help = "    compile-rst : Compile the RST source files into HTML."


compile-rst : build-sources
	$(PYVENV)/bin/python $$(pwd)/bin/compile_rst_sources.py -v $(RST_SOURCES)
	date > compile-rst
