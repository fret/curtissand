# Makefile for Website
#
# https://bitbucket.org/fret/website.git
#
# Copyright 2018-2020 Curtis Sand
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


static-help = "    static : Copy files from the static source into DIST."
images-help = "    images : Copy image files into DIST."


static : dist cssdist jsdist
	test -d $(STATIC) && rsync -haP --no-whole-file --inplace $(STATIC)/* $(STATIC_DIST)/ || true
	date > static

.PHONY: images
images : dist link-sources
	# test -d $(IMAGES) && ln -s $$(readlink $(IMAGES)) $(DIST)/images || true
	date > imgs
