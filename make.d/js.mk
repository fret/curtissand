# Makefile for Website
#
# https://bitbucket.org/fret/website.git
#
# Copyright 2018-2020 Curtis Sand
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


clean-js-help = "    clean-js : Remove any built Javascript files."
js-help = "    js : Use rollup and babel to compile javascript sources."
jslint-help = "    jslint : Use eslint to check the javascript style."
bootstrap-help = "    bootstrap : Copy the required bootstrap files to DIST."
jquery-help = "    jquery : Copy the required jquery files to DIST."
jsdist-help = "    jsdist : Create the Javascript directory in DIST."


.PHONY: clean-js
clean-js:
	rm -rf $(JS_DIST) js bootstrapjs jqueryjs jsdist

jsdist : dist
	mkdir -p $(JS_DIST)
	date > jsdist

bootstrap : jsdist
	cp $(BOOTSTRAP_JS) $(JS_DIST)
	date > bootstrapjs

jquery : jsdist
	cp $(JQUERY_JS) $(JS_DIST)
	date > jqueryjs

js : jsdist bootstrap jquery
	npx --prefix $(NODE_PKG) rollup -c
	date > js

.PHONY: jslint
jslint :
	$(ESLINT) $(JS)/*
