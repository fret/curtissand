# Makefile for Website
#
# https://bitbucket.org/fret/website.git
#
# Copyright 2018-2020 Curtis Sand
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Pages Module : Build HTML Pages out of Jinja2 templates using Skabelon.

# To enable this module in your project add the dependencies listed below to
# your other existing targets and add the variables listed below to your top
# level makefile. Also add
# "git+git://github.com/fretboardfreak/skabelon.git@master" to the
# $(PYTHON_REQUIRES) file and rerun the install-py target.
#
# Dependencies:
#
# - all : dist-pages
#
# Variables:
#
# - PAGES_BUILD = $(BUILD)/pages
# - PAGES_DIST = $(DIST)/pages
# - PAGES_DISPATCH = $(SRC)/skabelon/dispatch.py

build-pages-help = "    build-pages : Build the html pages from the compiled sources."
dist-pages-help = "    dist-pages : Move built pages into the dist dir."


build-pages : build build-sources compile-rst
	mkdir -p $(PAGES_BUILD)
	$(PYVENV)/bin/skabelon --templates $(HTML) \
		--dispatch $(PAGES_DISPATCH) \
		--dispatch-opt sources:$(SOURCES_BUILD) \
		--dispatch-opt dest:$(PAGES_BUILD) \
		--dispatch-opt host:$(HOST) \
		--dispatch-opt metadata:$(SOURCES_BUILD)/metadata.json \
		--dispatch-opt images:$(IMAGES)
	date > build-pages

dist-pages : build-pages dist
	mkdir -p $(PAGES_DIST)
	rsync -haL $(PAGES_BUILD)/* $(PAGES_DIST)
	date > dist-pages
