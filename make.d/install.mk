# Makefile for Website
#
# https://bitbucket.org/fret/website.git
#
# Copyright 2018-2020 Curtis Sand
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


clean-npm-help = "    clean-npm : Remove the local installed npm modules."
clean-py-help = "    clean-py : Remove the local installed python modules and venv."
install-help = "    install : install development packages."
install-npm-help = "    install-npm : install NPM packages."
install-py-help = "    install-py : install Python packages."
python-venv-help = "    python-venv : Create a Python 3 Virtual Environment."


.PHONY: clean-npm
clean-npm :
	rm -rf $(NODE) install-npm install

.PHONY: clean-py
clean-py :
	rm -rf $(PYVENV) install-py python-venv install

install : install-npm install-py
	date > install

install-npm :
	npm --prefix $(NODE_PKG) install
	date > install-npm

install-py : python-venv
	test -f $(PYTHON_REQUIRES) && \
		$(PYVENV)/bin/pip install -r $(PYTHON_REQUIRES) || true
	date > install-py

python-venv :
	test -f $(PYTHON_REQUIRES) && python3 -m venv $(PYVENV) || true
	test -f $(PYTHON_REQUIRES) && $(PYVENV)/bin/pip install --upgrade pip || true
	date > python-venv
