# Makefile for Website
#
# https://bitbucket.org/fret/website.git
#
# Copyright 2018-2020 Curtis Sand
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Sources Module : Link to source files stored in an external location

# To enable this module in your project add the dependencies listed below to
# your other existing targets and add the variables listed below to your top
# level makefile.
#
# Dependencies:
#
# - all : dist-sources
# - clean : clean-sources
# - images : link-sources
#
# Variables:
#
# - SOURCES = $$(pwd)/sources
# - SOURCES_BUILD = $(BUILD)/sources
# - SOURCES_DIST = $(DIST)/sources
# - SOURCES_CMD = ... # the command(s) to run to create the links

link-sources-help = "    link-sources : Create symlinks to media or other source files."
clean-sources-help = "    clean-sources : Remove media and source file symlinks."
build-sources-help = "    build-sources : Copy all files needed for "
dist-sources-help = "    dist-sources : Move built sources into the dist dir."


link-sources :
	mkdir -p $(SOURCES)
	$(SOURCES_CMD)
	date > link-sources

.PHONY: clean-sources
clean-sources :
	rm -r $(SOURCES)
	rm $(SOURCES_EXTRA_CLEAN)
	rm -f link-sources

build-sources : build link-sources
	mkdir -p $(SOURCES_BUILD)
	rsync -haL --exclude "*.txt" $(SOURCES)/* $(SOURCES_BUILD)
	date > build-sources

dist-sources : compile-rst dist
	mkdir -p $(SOURCES_DIST)
	rsync -haLP $(SOURCES_BUILD)/* $(SOURCES_DIST)
	date > dist-sources
