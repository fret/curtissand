# Makefile for Website
#
# https://bitbucket.org/fret/website.git
#
# Copyright 2018-2020 Curtis Sand
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Lightbox Module : Install Lightbox for use in your project.

# Lightbox Homepage: http://lokeshdhakar.com/projects/lightbox2/

# To enable this module in your project add the dependencies listed below to
# your other existing targets and add the variables listed below to your top
# level makefile.
#
# Dependencies:
#
# - all : lightbox
#
# Variables:
#
# - LIGHTBOX_CSS = $(NODE)/lightbox2/dist/css/lightbox.min.css
# - LIGHTBOX_JS = $(NODE)/lightbox2/dist/js/lightbox.min.*
# - LIGHTBOX_IMAGES = $(NODE)/lightbox2/dist/images/*


lightbox-help = "    lightbox : Copy the lightbox file from NPM into DIST."


lightbox : cssdist jsdist images
	cp $(LIGHTBOX_CSS) $(CSS_DIST)
	cp $(LIGHTBOX_JS) $(JS_DIST)
	# put the four lightbox images into images dir
	# since dist/images is a symlink these files will persist, oh well
	cp $(LIGHTBOX_IMAGES) $(IMAGES)
	date > lightbox
