#!/bin/bash

# link sources for the obsidian host
SOURCES=$(pwd)/sources
_FRET=$(pwd)/../fret
ln -s -t ${SOURCES} ${_FRET}/blog ;
ln -s -t ${SOURCES} ${_FRET}/ref ;
ln -s -t ${SOURCES} ${_FRET}/galleries ;
ln -s -t ${SOURCES} ${_FRET}/about.rst ;
ln -s -t ${SOURCES} ${_FRET}/projects.rst ;

ln -s /home/csand/storage/pics/website/cs $(pwd)/images;
